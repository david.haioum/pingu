
//************************SYSTEM INCLUDES********************************

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h> /* pour memset */
#include <netinet/in.h> /* pour struct sockaddr_in */
#include <arpa/inet.h> /* pour htons et inet_aton */
#include <unistd.h> /* pour sleep */
#include <sys/time.h>
#include <time.h>
#include <errno.h> 
#include <stdbool.h>

//************************PERSONNALIZE INCLUDES********************************

#include "../../include/server/server_structures.h"
#include "../../include/server/server.h"
#include "../../include/server/clientManager.h"
#include "../../include/server/server_protocol.h"


int main(int argc, char* argv[]){
    srand(time(0)); /* Initialize rand() */
    
    Server s; /* Our server struct */
    Client* ptrLlClient = NULL; /* Linked client list */

    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    FILE* fichier = NULL;

    fichier = fopen("server.conf", "r+");
    int MAX_NUMBER_CLIENTS_IN_CHAT;
    int MAX_NUMBER_CLIENTS_IN_QUEUE;
    int PORT_MIN;
    int PORT_MAX;
    int LOGIN_MAX_LENGTH;
    int LOGIN_MIN_LENGTH;
    int LG_MESSAGE;

    char HELLO_MSG[256];
    if (fichier != NULL)
    {
        

        fscanf(fichier, "SERVER PARAMETERS\n\nMax number of clients in chat: %d\nMax number of clients in queue: %d\n", &MAX_NUMBER_CLIENTS_IN_CHAT,&MAX_NUMBER_CLIENTS_IN_QUEUE);
        fscanf(fichier, "Min Port: %d\nMax Port: %d\nMax Login Length: %d\nMin Login Length: %d\nMax Message length: %d\nHello message: ",
        &PORT_MIN,
        &PORT_MAX,
        &LOGIN_MAX_LENGTH,
        &LOGIN_MIN_LENGTH,
        &LG_MESSAGE);
        fgets (HELLO_MSG , 256 , fichier);
        fclose(fichier);
    }
    else
    {
        // On affiche un message d'erreur si on veut
        printf("[ERROR]: Unable to open server.conf  \n");
        exit(-1);
    }

    int client_socket_in_chat[MAX_NUMBER_CLIENTS_IN_CHAT]; /* contains sockets of logged clients */
    int client_socket_in_queue[MAX_NUMBER_CLIENTS_IN_QUEUE]; /* contains sockets of clients who're in lobby (when a chat is protected by password) */

    int addrlen, activity , valread;
    int new_socket; /* if newcomming connection his socket descriptor will be stocked temporally in this var */
    
    int sd; /* current socket descriptor */
    int max_sd; /* max socket descriptor */
    int master_socket;
    struct sockaddr_in address; 
    char welcome_message[256];
    char serverAnswer[256];

    char username[255];
    char client_input_Password[256];

    char buffer[LG_MESSAGE];    
    
    initServer(&s, argc, argv); /*init the server */

    strcpy(client_input_Password,""); /* By default set the password to void string */

    master_socket = s.sockfd; /* Init master socket */
    addrlen = sizeof(struct sockaddr_in);
    fd_set readfds;

    //initialise all client_socket_in_chat[] to 0 so not checked  
    for (int i = 0; i < MAX_NUMBER_CLIENTS_IN_CHAT; i++)
    {   
        client_socket_in_chat[i] = 0;   
    }

    //initialise all client_socket_in_lobby[] to 0 so not checked  
    for (int i = 0; i < MAX_NUMBER_CLIENTS_IN_QUEUE; i++)   
    {   
        client_socket_in_queue[i] = 0;   
    }
    
    printf("Attente d'une demande de connexion (quitter avec Ctrl-C)\n\n");
    while (1)
    {

        FD_ZERO(&readfds); /* clear the socket set */
        FD_SET(master_socket, &readfds);   /* add master socket to set */

        max_sd = master_socket; /* Init the max socket descriptor */
             
        //add child sockets to set  
        for (int i = 0 ; i < MAX_NUMBER_CLIENTS_IN_QUEUE ; i++)   
        {

            sd = client_socket_in_queue[i]; /* socket descriptor */

            //if valid socket descriptor then add to read list  
            if(sd > 0)   
                FD_SET( sd , &readfds);   
                 
            //highest file descriptor number, need it for the select function  
            if(sd > max_sd)   
                max_sd = sd;   
        }
        for (int i = 0 ; i < MAX_NUMBER_CLIENTS_IN_CHAT ; i++)   
        {
            sd = client_socket_in_chat[i]; /*current socket descriptor */
                 
            //if valid socket descriptor then add to read list  
            if(sd > 0)   
                FD_SET( sd , &readfds);   
                 
            //highest file descriptor number, need it for the select function  
            if(sd > max_sd)   
                max_sd = sd;   
        }
        
        //wait for an activity on one of the sockets , timeout is NULL ,  
        //so wait indefinitely
        activity = select( max_sd + 1 , &readfds , NULL , NULL ,NULL);   
       
        if ((activity < 0) && (errno!=EINTR))   
        {   
            printf("select error");   
        }

        //If something happened on the master socket ,  
        //then its an incoming connection # WARNING A new foe has appeared
        if (FD_ISSET(master_socket, &readfds))
        {
            /* accept the connection */
            if ((new_socket = accept(master_socket,  
                    (struct sockaddr *)&address, (socklen_t*)&addrlen))<0)   
            {
                perror("accept");   
                exit(EXIT_FAILURE);   
            }
            /* DISABLE !version VERIFICATION
            sprintf(serverAnswer,"!version");
            send(new_socket,serverAnswer,sizeof(serverAnswer),0);

            read( new_socket , buffer, LG_MESSAGE);
            char version[256];
            sprintf(version,"!version %s", SERVER_VERSION);
            if(!strcmp(version,buffer)){*/
                /* If server has no password protection OR if client in lobby has enter a good password*/
                sesamGate:if(!strcmp(s.password,client_input_Password)){

                    struct timeval timeout;      
                    timeout.tv_sec = 0;
                    timeout.tv_usec = 500;

                    if (setsockopt (new_socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,
                                sizeof(timeout)) < 0)
                        perror("setsockopt failed\n");

                    if (setsockopt (new_socket, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout,
                                sizeof(timeout)) < 0)
                        perror("setsockopt failed\n");
                    
                    //reset the client_input_Password
                    strcpy(client_input_Password,"");

                    sprintf(username,"anon%d",rand() % 100000); //add random username to the var

                    printf("[LOG][%d-%d-%d %d:%d:%d]: New connection , username: %s socket fd is %d , ip is : %s , port : %d\n" ,
                        tm.tm_year + 1900,
                        tm.tm_mon + 1,
                        tm.tm_mday,
                        tm.tm_hour,
                        tm.tm_min,
                        tm.tm_sec,
                        username,
                        new_socket ,
                        inet_ntoa(address.sin_addr) ,
                        ntohs (address.sin_port));   
                    
                    //If there is any user create a new pointer to a client linked list
                    if(ptrLlClient == NULL){
                        ptrLlClient = createClient(username,inet_ntoa(address.sin_addr),new_socket);

                        //if createClient didn't worked, reject the connection
                        if(ptrLlClient == NULL){
                            sprintf(serverAnswer,"!error Username Taken.");
                            send(new_socket,serverAnswer,sizeof(serverAnswer),0);
                            close(new_socket);
                            new_socket = 0;
                        }

                    }

                    //If there is already at least one user create a new link
                    else{
                        //If chat is not full: number of clients in our linked list inferior at the MAX_NUMBER_CLIENTS_IN_CHAT const
                        if( countLlClient(ptrLlClient) < MAX_NUMBER_CLIENTS_IN_CHAT){
                            Client* current = ptrLlClient;
                            while(current){
                                current = current->next;
                            }
                            if(addClient(&ptrLlClient,createClient(username,inet_ntoa(address.sin_addr),new_socket)) == -1){
                                
                                sprintf(serverAnswer,"!error Username Taken.");
                                send(new_socket,serverAnswer,sizeof(serverAnswer),0);
                                close(new_socket);
                                new_socket = 0;
                            }
                        }

                        //chat full, reject the connection
                        else{
                            sprintf(serverAnswer,"!error Chat full.");
                            send(new_socket,serverAnswer,sizeof(serverAnswer),0);
                            close(new_socket);
                            new_socket = 0;
                        }
                    }
                }
                /* If server has password protection */
                else{
                    
                    //count the number of people in queue
                    int number_client_in_queue = 0;
                    for(int i = 0; i < MAX_NUMBER_CLIENTS_IN_QUEUE; i++){
                        if(client_socket_in_queue[i] != 0){
                            number_client_in_queue++;
                        }
                    }

                    //If lobby is not full
                    if( number_client_in_queue < MAX_NUMBER_CLIENTS_IN_QUEUE){
                        
                        //Send greeting message and ask password
                        sprintf(welcome_message,"!message SYSTEM This chat is protected by password. To enter, type the password.");
                        if( send(new_socket, welcome_message, sizeof(welcome_message), 0) != sizeof(welcome_message) )   
                        {   
                            perror("[ERROR]: send");   
                        }

                        //add new socket to array of sockets 
                        for (int i = 0; i < MAX_NUMBER_CLIENTS_IN_QUEUE; i++)   
                        {   
                            //if position is empty  
                            if( client_socket_in_queue[i] == 0 )   
                            {   
                                client_socket_in_queue[i] = new_socket;  
                                    
                                break;   
                            }
                        }
                        new_socket = 0;
                    }
                    //If lobby is full, reject the connection
                    else{
                        sprintf(serverAnswer,"!error Lobby full.");
                        send(new_socket,serverAnswer,sizeof(serverAnswer),0);
                        close(new_socket);
                        new_socket = 0;
                    }
                }
                if(new_socket){
                    //send new connection greeting message
                    sprintf(welcome_message,"!message SYSTEM Welcome in the %s chat !",s.name);
                    if( send(new_socket, welcome_message, sizeof(welcome_message), 0) != sizeof(welcome_message) )   
                    {
                        perror("[ERROR]: send");   
                    }
        
                }
                    
                //add new socket to array of sockets  
                for (int i = 0; i < MAX_NUMBER_CLIENTS_IN_CHAT; i++)   
                {   
                    //if position is empty  
                    if( client_socket_in_chat[i] == 0 )   
                    {   
                        client_socket_in_chat[i] = new_socket;  
                        break;
                    }
                }
                for (int j = 0; j < MAX_NUMBER_CLIENTS_IN_CHAT; j++)   
                {
                    if( client_socket_in_chat[j] != 0){
                        Client* current = ptrLlClient;
                        if(ptrLlClient->next){
                            strcpy(serverAnswer,"!list myself");
                        }
                        else{
                            strcpy(serverAnswer,"!list myself|");
                        }

                        while(current){
                            if(findClient(ptrLlClient,createSearchStruct(NULL,NULL,client_socket_in_chat[j]))){
                                if(current != findClient(ptrLlClient,createSearchStruct(NULL,NULL,client_socket_in_chat[j]))[0]){
                                    strcat(serverAnswer,"|");
                                    strcat(serverAnswer,current->login);
                                }
                            }
                            current = current->next;
                        }
                        send(client_socket_in_chat[j],serverAnswer,sizeof serverAnswer,0);
                    }
                }
                /* DISABLE !VERSION VERIFICATION
            }
            else{
                sprintf(serverAnswer,"!error Wrong client version, need version %s.",SERVER_VERSION);
                send(new_socket,serverAnswer,sizeof(serverAnswer),0);
                close(new_socket);
                new_socket = 0;
            }
            */
        }   
             
        //else process some IO operation on clients in chat
        for (int i = 0; i < MAX_NUMBER_CLIENTS_IN_CHAT; i++)   
        {   
            //set the current socket
            sd = client_socket_in_chat[i];   
            
            //check if is there is activity on the current socket
            if (FD_ISSET( sd , &readfds))   
            {   
                //Check if it was for closing , and also read the  
                //incoming message
                if ((valread = read( sd , buffer, LG_MESSAGE)) == 0)   
                {

                    //Somebody disconnected , get his details and print  
                    getpeername(sd , (struct sockaddr*)&address , (socklen_t*)&addrlen);
                    printf("[LOG][%d-%d-%d %d:%d:%d]: %s disconnected , ip %s , port %d \n" ,
                        tm.tm_year + 1900,
                        tm.tm_mon + 1,
                        tm.tm_mday,
                        tm.tm_hour,
                        tm.tm_min,
                        tm.tm_sec,
                        findClient(ptrLlClient,createSearchStruct(NULL,inet_ntoa(address.sin_addr),sd))[0]->login ,
                        inet_ntoa(address.sin_addr),
                        ntohs(address.sin_port));
                    
                    

                    //Close the socket and mark as 0 in list for reuse 
                    
                    
                    for (int j = 0; j < MAX_NUMBER_CLIENTS_IN_CHAT; j++)   
                    {
                        if( client_socket_in_chat[j] != 0){
                            Client* current = ptrLlClient;
                            if(ptrLlClient->next)
                                strcpy(serverAnswer,"!list myself");
                            else
                                strcpy(serverAnswer,"!list myself|");
                            while(current){
                                
                                if(current != findClient(ptrLlClient,createSearchStruct(NULL,NULL,client_socket_in_chat[j]))[0] && current != findClient(ptrLlClient,createSearchStruct(NULL,NULL,client_socket_in_chat[i]))[0]){
                                    strcat(serverAnswer,"|");
                                    strcat(serverAnswer,current->login);
                                }
                                
                                current = current->next;
                            }
                            send(client_socket_in_chat[j],serverAnswer,sizeof serverAnswer,0);
                        }
                    }
                    client_socket_in_chat[i] = 0;
                    removeClient(&ptrLlClient,findClient(ptrLlClient,createSearchStruct(NULL,inet_ntoa(address.sin_addr),sd))[0]);
                    close( sd );
                      
                }   
                     
                //process the message readed
                else 
                {
                    //set the string terminating NULL byte on the end  
                    //of the data read
                    buffer[valread-1] = '\0';
                    for(int i = 0; buffer[i] != '\0';i++){
                        if(buffer[i] == 16){
                            buffer[i] = '\0';
                        }
                    }
                    if(strcmp(buffer,""))
                        processData(sd,&ptrLlClient,buffer,client_socket_in_chat);
                }
            }   
        }
        //else its some IO operation on clients in lobby
        for (int i = 0; i < MAX_NUMBER_CLIENTS_IN_QUEUE; i++)   
        {   
            sd = client_socket_in_queue[i];   
                 
            if (FD_ISSET( sd , &readfds))   
            {   
                //Check if it was for closing , and also read the  
                //incoming message  
                if ((valread = read( sd , buffer, LG_MESSAGE)) == 0)   
                {   
                    //Somebody disconnected , get his details and close
                    getpeername(sd , (struct sockaddr*)&address , (socklen_t*)&addrlen);
                    client_socket_in_queue[i] = 0;
                    close( sd );   
                      
                }   
                     
                //Someone who's trying to enter the server with password
                else 
                {
                    //set the string terminating NULL byte on the end  
                    //of the data read
                    buffer[valread-1] = '\0';
                    //Put his input in the client_input_Password
                    strcpy(client_input_Password,buffer);

                    //if the password is good
                    if(!strcmp(s.password,client_input_Password)){

                        new_socket = client_socket_in_queue[i];
                        client_socket_in_queue[i] = 0;
                        memset(buffer,0,sizeof(buffer));
                        goto sesamGate;
                        
                        
                    }
                    //if it's not the good password
                    else{
                        if(strstr(buffer,"!message")){
                            sprintf(serverAnswer,"!error Wrong password.");
                            send(sd , serverAnswer , sizeof(serverAnswer) , 0 );
                        }
                        
                    }
                }
            }   
        }   
    }   
    
    return EXIT_SUCCESS;
}
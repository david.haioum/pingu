
//************************SYSTEM INCLUDES********************************

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h> /* pour memset */
#include <netinet/in.h> /* pour struct sockaddr_in */
#include <arpa/inet.h> /* pour htons et inet_aton */
#include <unistd.h> /* pour sleep */
#include <stdbool.h>
#include <sys/time.h>
#include <time.h>

//************************PERSONALIZE INCLUDES********************************

#include "../../include/server/server_structures.h"
#include "../../include/server/server.h"
#include "../../include/match.h"
#include "../../include/server/clientManager.h"
#include "../../include/server/server_protocol.h"


void displayHelpCommand()
{

    FILE* fichier = NULL;

    fichier = fopen("server.conf", "r+");
    int MAX_NUMBER_CLIENTS_IN_CHAT;
    int MAX_NUMBER_CLIENTS_IN_QUEUE;
    int PORT_MIN;
    int PORT_MAX;
    int LOGIN_MAX_LENGTH;
    int LOGIN_MIN_LENGTH;
    int LG_MESSAGE;

    char HELLO_MSG[256];
    if (fichier != NULL)
    {
        

        fscanf(fichier, "SERVER PARAMETERS\n\nMax number of clients in chat: %d\nMax number of clients in queue: %d\n", &MAX_NUMBER_CLIENTS_IN_CHAT,&MAX_NUMBER_CLIENTS_IN_QUEUE);
        fscanf(fichier, "Min Port: %d\nMax Port: %d\nMax Login Length: %d\nMin Login Length: %d\nMax Message length: %d\nHello message: ",
        &PORT_MIN,
        &PORT_MAX,
        &LOGIN_MAX_LENGTH,
        &LOGIN_MIN_LENGTH,
        &LG_MESSAGE);
        fgets (HELLO_MSG , 256 , fichier);
        fclose(fichier);
    }
    else
    {
        // On affiche un message d'erreur si on veut
        printf("[ERROR]: Unable to open server.conf  \n");
        exit(-1);
    }

    printf("\n--------------HELP--------------\n");
    printf("\nTypical use: ./mainServer [--help | %s <PORT_NUMBER> %s <SALOON_NAME>]\n",PORT_ARG,SERVER_NAME_ARG);
    printf("\n%s <PORT_NUMBER>  | The server will listen through this port (%d < your_port < %d)",PORT_ARG,PORT_MIN,PORT_MAX);
    printf("\n%s <SALOON_NAME>  | Set the Irc Saloon name (Yahooo, hands up Cowboy !)\n",SERVER_NAME_ARG);
    printf("\n%s <PASSWORD>  | Set a password on the server in order to filter users\n\n",PASSWORD_ARG);
}

void initServer(Server* s,int argc, char* argv[])
{
    int port;
    
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    
    strcpy(s->password,"");
    bool is_help_arg = false;
    bool is_server_name_arg = false;
    bool is_port_arg = false;
    bool is_password_arg = false;

    FILE* fichier = NULL;

    fichier = fopen("server.conf", "r+");
    int MAX_NUMBER_CLIENTS_IN_CHAT;
    int MAX_NUMBER_CLIENTS_IN_QUEUE;
    int PORT_MIN;
    int PORT_MAX;
    int LOGIN_MAX_LENGTH;
    int LOGIN_MIN_LENGTH;
    int LG_MESSAGE;

    char HELLO_MSG[256];
    if (fichier != NULL)
    {
        

        fscanf(fichier, "SERVER PARAMETERS\n\nMax number of clients in chat: %d\nMax number of clients in queue: %d\n", &MAX_NUMBER_CLIENTS_IN_CHAT,&MAX_NUMBER_CLIENTS_IN_QUEUE);
        fscanf(fichier, "Min Port: %d\nMax Port: %d\nMax Login Length: %d\nMin Login Length: %d\nMax Message length: %d\nHello message: ",
        &PORT_MIN,
        &PORT_MAX,
        &LOGIN_MAX_LENGTH,
        &LOGIN_MIN_LENGTH,
        &LG_MESSAGE);
        fgets (HELLO_MSG , 256 , fichier);
        fclose(fichier);
    }
    else
    {
        // On affiche un message d'erreur si on veut
        printf("[ERROR]: Unable to open server.conf  \n");
        exit(-1);
    }

    if( argc != (EXPECTED_ARGUMENT_NUMBER*2+1) && argc != (EXPECTED_ARGUMENT_NUMBER*3+1)  && argc != 2){
        printf("\n[INIT]: error missing arguments; Use --help argument for more details.\n\n");
        exit(-1);
    }

    for(int i = 1; i < argc; i++)
    {
        if(!strcmp(argv[i], HELP_ARG) && is_server_name_arg == false && is_port_arg == false && is_password_arg == false){
            is_help_arg = true;
        }
        else if(!strcmp(argv[i], HELP_ARG) && (is_server_name_arg == true || is_port_arg == true || is_password_arg == true) && argc != 2){
            printf("\n[ERROR]: not a valid argument (mixing help with other options) ! Use --help argument for more details.\n\n");
            exit(-1);
        }
        else if(!strcmp(argv[i], PORT_ARG) && is_help_arg == false && argc != 2){
            is_port_arg = true;
            i++;
        }
        else if(!strcmp(argv[i], PORT_ARG) && is_help_arg == true && argc != 2){
            printf("\n[ERROR]: not a valid argument (mixing help with other options) ! Use --help argument for more details.\n\n");
            exit(-1);
        }
        else if(!strcmp(argv[i], SERVER_NAME_ARG) && is_help_arg == false && argc != 2){
            is_server_name_arg = true;
            i++;
        }
        else if(!strcmp(argv[i], SERVER_NAME_ARG) && is_help_arg == true && argc != 2){
            printf("\n[ERROR]: not a valid argument (mixing help with other options) ! Use --help argument for more details.\n\n");
            exit(-1);
        }
        else if(!strcmp(argv[i], PASSWORD_ARG) && is_help_arg == false && argc != 2){
            is_password_arg = true;
            i++;
        }
        else if(!strcmp(argv[i], PASSWORD_ARG) && is_help_arg == true && argc != 2){
            printf("\n[ERROR]: not a valid argument (mixing help with other options) ! Use --help argument for more details.\n\n");
            exit(-1);
        }
        else{
            printf("\n[ERROR]: not a valid argument ! Use --help argument for more details.\n\n");
            exit(-1);
        }
    }

    if((is_port_arg == true && is_server_name_arg == false) || (is_port_arg == false && is_server_name_arg == true)){
        printf("\n[ERROR]: not a valid argument ! Use --help argument for more details.\n\n");
        exit(-1);
    }

    for(int i = 1; i < argc; i++)
    {
        if( !strcmp(argv[i], PORT_ARG) ){
            port = atoi(argv[i+1]);
            if( port > PORT_MAX || port < PORT_MIN){
                printf("\n[ERROR]: port must be between %d and %d \n", PORT_MIN, PORT_MAX);
                printf("\nExit...\n\n");
                exit(-1);
            }
            s->port = atoi(argv[i+1]);

        }
        else if(!strcmp(argv[i], HELP_ARG)){
            displayHelpCommand();
            exit(0);
        }
        else if(!strcmp(argv[i], PASSWORD_ARG)){
            strcpy(s->password, "!message * ");
            strcat(s->password,argv[i+1]);
        }
        else if(!strcmp(argv[i], SERVER_NAME_ARG) ){
            s->name = argv[i+1];
        }              
    }
    
    s->listeOfClient = NULL;

    s->sockfd = 0;

    printf("\nSUM UP:\n");
    printf("SERVER_VERSION: %s\n",SERVER_VERSION);
    printf("PORT: %d\n", s->port);
    printf("Max number of clients in chat: %d\n", MAX_NUMBER_CLIENTS_IN_CHAT);
    printf("Max number of clients in queue: %d\n", MAX_NUMBER_CLIENTS_IN_QUEUE);
    printf("Max Login Length: %d\n", LOGIN_MAX_LENGTH);
    printf("Min Login Length: %d\n", LOGIN_MIN_LENGTH);
    printf("Max Message length: %d\n", LG_MESSAGE);
    printf("Hello message: '%s'\n", HELLO_MSG);

    if(!strcmp(s->password,"")){
        printf("PASSWORD ?: NO\n\n");
    }
    else{
        printf("PASSWORD ?: YES(%s)\n\n",s->password);
    }

    int opt = 1;   
    int master_socket;
    struct sockaddr_in address;  
         
    //create a master socket  
    if( (master_socket = socket(AF_INET , SOCK_STREAM , 0)) == 0)   
    {   
        perror("[ERROR]: socket");   
        exit(EXIT_FAILURE);   
    }

    //set master socket to allow multiple connections ,  
    //this is just a good habit, it will work without this  
    if( setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt,  
          sizeof(opt)) < 0 )   
    {   
        perror("[ERROR]: setsockopt");   
        exit(EXIT_FAILURE);   
    }   
     
    //type of socket created  
    address.sin_family = AF_INET;   
    address.sin_addr.s_addr = INADDR_ANY;   
    address.sin_port = htons( s->port );   
         
    //bind the socket to localhost port 8888  
    if (bind(master_socket, (struct sockaddr *)&address, sizeof(address))<0)   
    {   
        perror("[ERROR]: bind");   
        exit(EXIT_FAILURE);   
    }   
    printf("[INIT]: Socket(port:%d) successfuly attached.\n", s->port);   
         
    //try to specify maximum of 100 pending connections for the master socket  
    if (listen(master_socket, 100) < 0)   
    {   
        perror("[ERROR]: listen");   
        exit(EXIT_FAILURE);   
    }

    // Fin de l'étape n°6 !
    printf("[INIT]: Socket(port:%d) put in passive listening.\n", s->port);

    printf("[LOG][%d-%d-%d %d:%d:%d]: Server initialized. \n\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    s->sockfd = master_socket;
}

void broadcastChat(char data[], int client_socket_in_chat[],Client* ptrLlClient, Client* exception){

    FILE* fichier = NULL;

    fichier = fopen("server.conf", "r+");
    int MAX_NUMBER_CLIENTS_IN_CHAT;
    int MAX_NUMBER_CLIENTS_IN_QUEUE;
    int PORT_MIN;
    int PORT_MAX;
    int LOGIN_MAX_LENGTH;
    int LOGIN_MIN_LENGTH;
    int LG_MESSAGE;

    char HELLO_MSG[256];
    if (fichier != NULL)
    {
        

        fscanf(fichier, "SERVER PARAMETERS\n\nMax number of clients in chat: %d\nMax number of clients in queue: %d\n", &MAX_NUMBER_CLIENTS_IN_CHAT,&MAX_NUMBER_CLIENTS_IN_QUEUE);
        fscanf(fichier, "Min Port: %d\nMax Port: %d\nMax Login Length: %d\nMin Login Length: %d\nMax Message length: %d\nHello message: ",
        &PORT_MIN,
        &PORT_MAX,
        &LOGIN_MAX_LENGTH,
        &LOGIN_MIN_LENGTH,
        &LG_MESSAGE);
        fgets (HELLO_MSG , 256 , fichier);
        fclose(fichier);
    }
    else
    {
        // On affiche un message d'erreur si on veut
        printf("[ERROR]: Unable to open server.conf  \n");
        exit(-1);
    }

    char serverAnswer[LG_MESSAGE];
    strcpy(serverAnswer,data);
    for(int i = 0; i < MAX_NUMBER_CLIENTS_IN_CHAT; i++){
        if(client_socket_in_chat[i] != 0){
            if(exception){
                if(findClient(ptrLlClient,createSearchStruct(NULL,NULL,client_socket_in_chat[i]))[0] != exception){
                    send(client_socket_in_chat[i] , serverAnswer , sizeof(serverAnswer) , 0 );
                }
            }
            else{
                send(client_socket_in_chat[i] , serverAnswer , sizeof(serverAnswer) , 0 );
            }
        }
    }
}

void broadcastLobby(char data[], int client_socket_in_lobby[]){

    FILE* fichier = NULL;

    fichier = fopen("server.conf", "r+");
    int MAX_NUMBER_CLIENTS_IN_CHAT;
    int MAX_NUMBER_CLIENTS_IN_QUEUE;
    int PORT_MIN;
    int PORT_MAX;
    int LOGIN_MAX_LENGTH;
    int LOGIN_MIN_LENGTH;
    int LG_MESSAGE;

    char HELLO_MSG[256];
    if (fichier != NULL)
    {
        

        fscanf(fichier, "SERVER PARAMETERS\n\nMax number of clients in chat: %d\nMax number of clients in queue: %d\n", &MAX_NUMBER_CLIENTS_IN_CHAT,&MAX_NUMBER_CLIENTS_IN_QUEUE);
        fscanf(fichier, "Min Port: %d\nMax Port: %d\nMax Login Length: %d\nMin Login Length: %d\nMax Message length: %d\nHello message: ",
        &PORT_MIN,
        &PORT_MAX,
        &LOGIN_MAX_LENGTH,
        &LOGIN_MIN_LENGTH,
        &LG_MESSAGE);
        fgets (HELLO_MSG , 256 , fichier);
        fclose(fichier);
    }
    else
    {
        // On affiche un message d'erreur si on veut
        printf("[ERROR]: Unable to open server.conf  \n");
        exit(-1);
    }

    char serverAnswer[LG_MESSAGE];
    strcpy(serverAnswer,data);
    for(int i = 0; i < MAX_NUMBER_CLIENTS_IN_QUEUE; i++){
        if(client_socket_in_lobby[i] != 0){
            send(client_socket_in_lobby[i] , serverAnswer , sizeof(serverAnswer) , 0 );
        }
    }
}

void processData(int socketDescriptor, Client** addPtrLlClient, char data[],int client_socket_in_chat[])
{

    FILE* fichier = NULL;

    fichier = fopen("server.conf", "r+");
    int MAX_NUMBER_CLIENTS_IN_CHAT;
    int MAX_NUMBER_CLIENTS_IN_QUEUE;
    int PORT_MIN;
    int PORT_MAX;
    int LOGIN_MAX_LENGTH;
    int LOGIN_MIN_LENGTH;
    int LG_MESSAGE;

    char HELLO_MSG[256];
    if (fichier != NULL)
    {
        

        fscanf(fichier, "SERVER PARAMETERS\n\nMax number of clients in chat: %d\nMax number of clients in queue: %d\n", &MAX_NUMBER_CLIENTS_IN_CHAT,&MAX_NUMBER_CLIENTS_IN_QUEUE);
        fscanf(fichier, "Min Port: %d\nMax Port: %d\nMax Login Length: %d\nMin Login Length: %d\nMax Message length: %d\nHello message: ",
        &PORT_MIN,
        &PORT_MAX,
        &LOGIN_MAX_LENGTH,
        &LOGIN_MIN_LENGTH,
        &LG_MESSAGE);
        fgets (HELLO_MSG , 256 , fichier);
        fclose(fichier);
    }
    else
    {
        // On affiche un message d'erreur si on veut
        printf("[ERROR]: Unable to open server.conf  \n");
        exit(-1);
    }

    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    
    Client* ptrLlClient = *addPtrLlClient;

    char publicMessage[LG_MESSAGE+LOGIN_MAX_LENGTH+2];/* Size: LOGIN_MAX_LENGTH(client username) + 2(: ) + LG_MESSAGE(message)*/
    char serverAnswer[LG_MESSAGE];
    int returnCode;

    if(data[0] != '!'){
        //Erreur commande ! inconnue: ex:fazfza
        sprintf(serverAnswer,"!error Invalid command.");
        send(socketDescriptor , serverAnswer ,sizeof(serverAnswer) , 0 );
    }
    else{
        if(!strcmp(data,QUIT_CMD)){
        
            printf("[LOG][%d-%d-%d %d:%d:%d]: %s disconnected , ip %s\n" ,
                tm.tm_year + 1900,
                tm.tm_mon + 1,
                tm.tm_mday,
                tm.tm_hour,
                tm.tm_min,
                tm.tm_sec,
                findClient(ptrLlClient,createSearchStruct(NULL,NULL,socketDescriptor))[0]->login ,
                findClient(ptrLlClient,createSearchStruct(NULL,NULL,socketDescriptor))[0]->ip);

            //Close the socket and mark as 0 in list for reuse 

            for (int j = 0; j < MAX_NUMBER_CLIENTS_IN_CHAT; j++)   
            {
                if( client_socket_in_chat[j] != 0){
                    Client* current = ptrLlClient;
                    if(ptrLlClient->next)
                        strcpy(serverAnswer,"!list myself");
                    else
                        strcpy(serverAnswer,"!list myself|");
                    while(current){
                        
                        if(current != findClient(ptrLlClient,createSearchStruct(NULL,NULL,client_socket_in_chat[j]))[0] && current != findClient(ptrLlClient,createSearchStruct(NULL,NULL,socketDescriptor))[0]){
                            strcat(serverAnswer,"|");
                            strcat(serverAnswer,current->login);
                        }
                        
                        current = current->next;
                    }
                    send(client_socket_in_chat[j],serverAnswer,sizeof serverAnswer,0);
                }
            }
            sprintf(serverAnswer,"!message SYSTEM %s disconnected.", findClient(ptrLlClient,createSearchStruct(NULL,NULL,socketDescriptor))[0]->login);
            for(int i = 0; i < MAX_NUMBER_CLIENTS_IN_CHAT; i++){
                if(client_socket_in_chat[i] == socketDescriptor){
                    client_socket_in_chat[i] = 0;
                    break;
                }
            }
    
            removeClient(addPtrLlClient,findClient(ptrLlClient,createSearchStruct(NULL,NULL,socketDescriptor))[0]);
            close( socketDescriptor );

            
            broadcastChat(serverAnswer,client_socket_in_chat,NULL,NULL);

            
        }
        else if(!strcmp(data,VERSION_CMD)){
            sprintf(serverAnswer,"!version Version actuelle: %s.",SERVER_VERSION);
            send(socketDescriptor,serverAnswer,sizeof(serverAnswer),0);
        }
        else if(!strcmp(data,LOGIN_CMD)){
            sprintf(serverAnswer,"!message SYSTEM Ton pseudo est %s.",findClient(ptrLlClient,createSearchStruct(NULL,NULL,socketDescriptor))[0]->login);
            send(socketDescriptor,serverAnswer,sizeof(serverAnswer),0);
        }
        else if(!strcmp(data,LIST_CMD)){
            Client* current = ptrLlClient;
            if(ptrLlClient->next)
                strcpy(serverAnswer,"!list myself");
            else
                strcpy(serverAnswer,"!list myself|");
            while(current){
                
                if(current != findClient(ptrLlClient,createSearchStruct(NULL,NULL,socketDescriptor))[0]){
                    strcat(serverAnswer,"|");
                    strcat(serverAnswer,current->login);
                }
                
                current = current->next;
            }
            send(socketDescriptor,serverAnswer,sizeof(serverAnswer),0);
        }
        else if(!strcmp(data,HELLO_CMD)){
            sprintf(serverAnswer,"!hello %s",HELLO_MSG);
            send(socketDescriptor,serverAnswer,sizeof(serverAnswer),0);
        }
        else{
            char *command = strtok(data, " ");
            

            if(command){
                char *arg1 = strtok(NULL, " ");

                if(arg1){
                    if(!strcmp(command,LOGIN_CMD)){
                        if(sizeof(arg1) <=LOGIN_MAX_LENGTH && sizeof(arg1) >= LOGIN_MIN_LENGTH){
                            if(findClient(ptrLlClient,createSearchStruct(NULL,NULL,socketDescriptor))){
                                char oldLogin[LOGIN_MAX_LENGTH];
                                strcpy(oldLogin,findClient(ptrLlClient,createSearchStruct(NULL,NULL,socketDescriptor))[0]->login );
                                returnCode = setClientLogin(findClient(ptrLlClient,createSearchStruct(NULL,NULL,socketDescriptor))[0],arg1,ptrLlClient);
                                if(returnCode > 0){
                                    sprintf(serverAnswer,"!message SYSTEM Changement de pseudo: %s -> %s",oldLogin,arg1);
                                    printf("[LOG][%d-%d-%d %d:%d:%d]: Changement de pseudo: %s -> %s.\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec,oldLogin,arg1);
                                    broadcastChat(serverAnswer,client_socket_in_chat,ptrLlClient,findClient(ptrLlClient,createSearchStruct(NULL,NULL,socketDescriptor))[0]);
                                    for (int j = 0; j < MAX_NUMBER_CLIENTS_IN_CHAT; j++)   
                                    {
                                        if( client_socket_in_chat[j] != 0){
                                            Client* current = ptrLlClient;
                                            if(ptrLlClient->next)
                                                strcpy(serverAnswer,"!list myself");
                                            else
                                                strcpy(serverAnswer,"!list myself|");
                                            while(current){
                                                
                                                if(current != findClient(ptrLlClient,createSearchStruct(NULL,NULL,client_socket_in_chat[j]))[0]){
                                                    strcat(serverAnswer,"|");
                                                    strcat(serverAnswer,current->login);
                                                }
                                                
                                                current = current->next;
                                            }
                                            send(client_socket_in_chat[j],serverAnswer,sizeof serverAnswer,0);
                                        }
                                    }
                                }
                                else if(returnCode == -2){
                                    sprintf(serverAnswer,"!error Username must contains only numbers and letters.");
                                    send(socketDescriptor,serverAnswer,sizeof(serverAnswer),0);
                                }
                                else if(returnCode == -1){
                                    sprintf(serverAnswer,"!error Username Taken.\n");
                                    send(socketDescriptor,serverAnswer,sizeof(serverAnswer),0);
                                }
                                else if(returnCode == -3){
                                    sprintf(serverAnswer,"!error Nom reserve (On touche pas au domaine des dieux).");
                                    send(socketDescriptor,serverAnswer,sizeof(serverAnswer),0);
                                }
                            }
                        }
                        else{
                            sprintf(serverAnswer,"!error Username non compris entre 3 et 100 caractères.");
                            send(socketDescriptor , serverAnswer ,sizeof(serverAnswer) , 0 );
                        }
                    }
                    else if(!strcmp(command,MESSAGE_CMD)){
                        char* arg2 = strtok(NULL, "\n");
                        
                        if(arg2){
                            
                            

                            Client* dest;

                            if(findClient(ptrLlClient,createSearchStruct(arg1,NULL,-1))){
                                dest = findClient(ptrLlClient,createSearchStruct(arg1,NULL,-1))[0];

                                printf("[LOG][%d-%d-%d %d:%d:%d]: %s(%s) sent to %s(%s): %s\n",
                                    tm.tm_year + 1900,
                                    tm.tm_mon + 1,
                                    tm.tm_mday,
                                    tm.tm_hour,
                                    tm.tm_min,
                                    tm.tm_sec,
                                    findClient(ptrLlClient,createSearchStruct(NULL,NULL,socketDescriptor))[0]->login,
                                    findClient(ptrLlClient,createSearchStruct(NULL,NULL,socketDescriptor))[0]->ip,
                                    dest->login,
                                    dest->ip,
                                    arg2);
                                
                                sprintf(serverAnswer,"!message PRIVATE %s->vous: %s",findClient(ptrLlClient,createSearchStruct(NULL,NULL,socketDescriptor))[0]->login,arg2);
                                send(dest->socketDiscussion,serverAnswer,sizeof(serverAnswer),0);
                                sprintf(serverAnswer,"!message PRIVATE vous->%s: %s",arg1,arg2);
                                send(socketDescriptor,serverAnswer,sizeof(serverAnswer),0);
                            }
                            else{
                                if(!strcmp(arg1,"*")){
                                    sprintf(publicMessage,"!message %s: %s",findClient(ptrLlClient,createSearchStruct(NULL,NULL,socketDescriptor))[0]->login, arg2);
                                    
                                    printf("[LOG][%d-%d-%d %d:%d:%d]: %s(%s) sent to @everyone: %s\n",
                                        tm.tm_year + 1900,
                                        tm.tm_mon + 1,
                                        tm.tm_mday,
                                        tm.tm_hour,
                                        tm.tm_min,
                                        tm.tm_sec,
                                        findClient(ptrLlClient,createSearchStruct(NULL,NULL,socketDescriptor))[0]->login,
                                        findClient(ptrLlClient,createSearchStruct(NULL,NULL,socketDescriptor))[0]->ip,
                                        arg2);
                                    //send to everyone

                                    while(strstr(publicMessage,"OwO")){
                                        strcpy(publicMessage,replace_str(publicMessage,"OwO","Mes parents me considere comme une deception"));
                                    }
                                    while(strstr(publicMessage,"0w0")){
                                        strcpy(publicMessage,replace_str(publicMessage,"0w0","Mes parents me considere comme une deception"));
                                    }
                                    while(strstr(publicMessage,"uwu")){
                                        strcpy(publicMessage,replace_str(publicMessage,"uwu","Mes parents me considere comme une deception"));
                                    }
                                    
                                    broadcastChat(publicMessage,client_socket_in_chat,ptrLlClient,findClient(ptrLlClient,createSearchStruct(NULL,NULL,socketDescriptor))[0]);
                                }
                                else{
                                    sprintf(serverAnswer,"!error L'utilisateur %s n'existe pas.",arg1);
                                    send(socketDescriptor , serverAnswer ,sizeof(serverAnswer) , 0 );
                                }
                            }
                        }
                        else{
                            //Erreur pas de deuxième arg: ex: !message [...]
                            sprintf(serverAnswer,"!error Invalid message syntax, no message.");
                            send(socketDescriptor , serverAnswer ,sizeof(serverAnswer) , 0 );
                        }
                    }
                    else{
                        //Erreur commande ! inconnue: ex:!fazfza dazdfza
                        
                        sprintf(serverAnswer,"!error Invalid command.");
                        send(socketDescriptor , serverAnswer ,sizeof(serverAnswer) , 0 );
                    }
                }
                else{
                    //Erreur commande ! inconnue: ex:'!<command> '
                        sprintf(serverAnswer,"!error Invalid command.");
                        send(socketDescriptor , serverAnswer ,sizeof(serverAnswer) , 0 );
                }
            }
            else{
                //Erreur commande ! inconnue: ex:!fazfza
                sprintf(serverAnswer,"!error Invalid command.");
                send(socketDescriptor , serverAnswer ,sizeof(serverAnswer) , 0 );
            }
        }
    }
}

//************************INCLUDES NORMALISES********************************

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//************************INCLUDES PERSONNALISES********************************

#include "../../../include/server/server_structures.h"
#include "../../../include/server/clientManager.h"
#include "../../../include/match.h"

int main(){

    printf("/****START TEST CREATE CLIENT****/\n\n");

    Client* ptrLlClient = createClient("Toto","192.168.1.2",0);

    printf("/****END TEST CREATE CLIENT****/\n\n");

    printf("/****START TEST ADD CLIENT TO LINKED LIST****/\n\n");

    addClient(&ptrLlClient,createClient("Atlas","192.168.1.3",1));
    
    addClient(&ptrLlClient,createClient("Atlas","192.168.1.4",2));

    addClient(&ptrLlClient,createClient("Atla)s","192.168.1.4",3));

    addClient(&ptrLlClient,createClient("Atlas2","192.168.1.5",4));
    
    addClient(&ptrLlClient,createClient("Atlas3","192.168.1.6",5));

    addClient(&ptrLlClient,createClient("Atlas8","192.168.1.4",6));
    
    addClient(&ptrLlClient,createClient("Atlas4","192.168.1.7",7));

    addClient(&ptrLlClient,createClient("Atlas5","192.168.1.8",8));

    printf("/****END TEST ADD CLIENT TO LINKED LIST****/\n\n");

    printf("/****START TEST REMOVE CLIENT****/\n\n");

    removeClient(&ptrLlClient,findClient(ptrLlClient,createSearchStruct("Atlas4",NULL,-1))[0]);

    printf("/****END TEST REMOVE CLIENT****/\n\n");

    printf("/****START TEST SHOW LINKED LIST****/\n\n");

    printLlClient(ptrLlClient);

    printf("/****END TEST SHOW LINKED LIST****/\n\n");

    printf("/****START TEST CREATE SEARCHSTRUCT****/\n\n");

    SearchStruct search = createSearchStruct("Atlas3",NULL,-1);

    printf("/****END TEST CREATE SEARCHSTRUCT****/\n\n");

    printf("/****START TEST FINDCLIENT WITH USERNAME NORM****/\n\n");

    Client** results = findClient(ptrLlClient,search);
    
    if(results){
        for(int i = 0; results[i]; i++){
            printClient(results[i],i+1);
        }
        free(results);
        results = NULL;
    }
    
    printf("/****END TEST FINDCLIENT WITH USERNAME NORM****/\n\n");

    printf("/****START TEST FINDCLIENT WITH ADDRESS NORM****/\n\n");

    results = findClient(ptrLlClient,createSearchStruct(NULL,"192.168.1.4",-1));
    
    if(results){
        for(int i = 0; results[i]; i++){
            printClient(results[i],i+1);
        }
        free(results);
        results = NULL;
    }

    
    
    printf("/****END TEST FINDCLIENT WITH ADRESS NORM****/\n\n");

    printf("/****START TEST FINDCLIENT WITH ANY NORM****/\n\n");

    int* socket = malloc(sizeof(int));
    *socket = 5000;

    results = findClient(ptrLlClient,createSearchStruct(NULL,NULL,-1));

    free(socket);
    socket = NULL;
    
    if(results){
        for(int i = 0; results[i]; i++){
            printClient(results[i],i+1);
        }
    }
    free(results);
    results = NULL;
    
    printf("/****END TEST FINDCLIENT WITH ANY NORM****/\n\n");

    printf("/****START TEST FREE LINKED LIST****/\n\n");

    printf("Linked List before function: %ld bytes (%d elements in linked list)\n", sizeof(ptrLlClient)*countLlClient(ptrLlClient), countLlClient(ptrLlClient));
    freeLinkedList(&ptrLlClient);
    printf("Linked List after function: %ld bytes (%d elements in linked list)\n",sizeof(ptrLlClient)*countLlClient(ptrLlClient), countLlClient(ptrLlClient));
    
    printf("/****END TEST FREE LINKED LIST****/\n\n");

    return EXIT_SUCCESS;
}
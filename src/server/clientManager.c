
//************************SYSTEM INCLUDES********************************

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//************************PERSONALIZE INCLUDES********************************

#include "../../include/server/server_structures.h"
#include "../../include/server/clientManager.h"
#include "../../include/match.h"
#include "../../include/server/server.h"

Client* createClient(char* login, char* ip, int socketDiscussion){
    
    FILE* fichier = NULL;

    fichier = fopen("server.conf", "r+");
    int MAX_NUMBER_CLIENTS_IN_CHAT;
    int MAX_NUMBER_CLIENTS_IN_QUEUE;
    int PORT_MIN;
    int PORT_MAX;
    int LOGIN_MAX_LENGTH;
    int LOGIN_MIN_LENGTH;
    int LG_MESSAGE;

    char HELLO_MSG[256];
    if (fichier != NULL)
    {
        

        fscanf(fichier, "SERVER PARAMETERS\n\nMax number of clients in chat: %d\nMax number of clients in queue: %d\n", &MAX_NUMBER_CLIENTS_IN_CHAT,&MAX_NUMBER_CLIENTS_IN_QUEUE);
        fscanf(fichier, "Min Port: %d\nMax Port: %d\nMax Login Length: %d\nMin Login Length: %d\nMax Message length: %d\nHello message: ",
        &PORT_MIN,
        &PORT_MAX,
        &LOGIN_MAX_LENGTH,
        &LOGIN_MIN_LENGTH,
        &LG_MESSAGE);
        fgets (HELLO_MSG , 256 , fichier);
        fclose(fichier);
    }
    else
    {
        // On affiche un message d'erreur si on veut
        printf("[ERROR]: Unable to open server.conf  \n");
        exit(-1);
    }
    
    //verify login is correct
    if( strlen(login) > LOGIN_MAX_LENGTH ){
        printf("\n[ERROR]: client login must be less than %d characters \n", LOGIN_MAX_LENGTH);
        return NULL;
    }
    //only letters and numbers
    if( match(login, "[^A-Za-z0-9]+") != 0){
        printf("\n[ERROR]: client must contains only numbers and letters \n");
        return NULL;
    }

    if( match(ip, "^([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5])).([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5])).([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5])).([0-9]|[1-9][0-9]|1([0-9][0-9])|2([0-4][0-9]|5[0-5]))$" ) == 0){
        printf("\n[ERROR]: ip address is wrong ! \n");
        return NULL;
    }
    
    

    Client* newClient = malloc(sizeof(Client));
    newClient->login = malloc(sizeof(char)*(strlen(login)+1));
    for(int i = 0 ; i < strlen(login); i++){
        newClient->login[i] = login[i];
    }
    newClient->login[strlen(login)] = '\0';
    newClient->ip = ip;
    newClient->socketDiscussion = socketDiscussion;
    newClient->next = NULL;
    return newClient;

}

int addClient(Client** addrptrLlClient,Client* newClient){
    Client* ptrLlClient = *addrptrLlClient;
    Client* tmp;

    if(newClient){
        if(ptrLlClient){
            Client* current = ptrLlClient;

            while(current){
                current = current->next;
            }
            current = ptrLlClient;
            
            while(current){
                if(!strcmp(current->login,newClient->login)){

                    //return -1 Error: Username Taken" to client
                    return -1;
                }
                if(!(current->next))
                    tmp = current;
                current = current->next;
            }
            //No problem username not taken return 1
            tmp->next = newClient;
            return 1;
        }
        else{
            *addrptrLlClient = newClient;
            return 1;
        }

    }
    else{
        printf("\n[ERROR]: trying to add NULL to Client Linked List");
        return -1;
    }
}

int freeLinkedList(Client** addrptrLlClient){
    Client* next;

    if(*addrptrLlClient){
        next = (*addrptrLlClient)->next;
        while(*addrptrLlClient){
            next = (*addrptrLlClient)->next;
            free(*addrptrLlClient);
            (*addrptrLlClient) = next;
        }
        return 1;
    }
    else{
        return -1;
        printf("\n[ERROR]: Client Linked List already free.\n");
    }
}
int countLlClient(Client* ptrLcClient){
    Client* current;
    int cpt;

    cpt = 0;
    current = ptrLcClient;
    while(current){
        cpt++;
        current = current->next;
    }

    return cpt;
}
int removeClient(Client** addPtrLlClient,Client* client){
    Client* ptrLlClient = *addPtrLlClient;
    Client* current = ptrLlClient;
    Client* tmp = NULL;
    switch (countLlClient(ptrLlClient))
    {
        case 0:
            printf("\n[ERROR]: inexistent Client Linked List (Null)\n");
            return -1;

        case 1:
            client = *addPtrLlClient;
            *addPtrLlClient = NULL;
            break;
        
        default:
            while(current){
                if(current){
                    if(current->next == client){
                        tmp = current;
                        break;
                    }
                }
                current = current->next;
            }
            if(tmp)
                tmp->next = tmp->next->next;
            else if(ptrLlClient == client)
                *addPtrLlClient = (*addPtrLlClient)->next;
            break;
    }

    if(client){
        free(client);
        client = NULL;
        return 1;
    }
    else{
        printf("\n[ERROR]: Trying to remove inexistent Client\n");
        return -1;
    }
    
}



Client** findClient(Client* ptrLlClient, SearchStruct norm){

    int expected_Norms, find_Norms, number_finded, cursor;
    Client* current;

    expected_Norms = 0;

    if(norm.login){
        expected_Norms++;
    }
    if(norm.ip){
        expected_Norms++;
    }
    if(norm.socketDiscussion){
        expected_Norms++;
    }
    number_finded = 0;
    current = ptrLlClient;
    while(current){
        find_Norms = 0;
        if(norm.login){
            if(!strcmp(norm.login, current->login)){
                find_Norms++;
            }
        }
        if(norm.ip){
            if(!strcmp(norm.ip,current->ip)){
                find_Norms++;
            }
        }
        if(norm.socketDiscussion){
            if((*norm.socketDiscussion) == current->socketDiscussion){
                find_Norms++;
            }
        }
        if(find_Norms == expected_Norms){
            number_finded++;
        }

        current = current->next;
    }
    if(!number_finded){
        return NULL;
    }
    Client** results = malloc(sizeof(Client)*(number_finded + 1));
    current = ptrLlClient;
    cursor = 0;
    
    while(current){
        find_Norms = 0;
        if(norm.login){
            if(!strcmp(norm.login, current->login)){
                find_Norms++;
            }
        }
        if(norm.ip){
            if(!strcmp(norm.ip,current->ip)){
                find_Norms++;
            }
        }
        if(norm.socketDiscussion){
            if((*norm.socketDiscussion) == current->socketDiscussion){
                find_Norms++;
            }
        }
        if(find_Norms == expected_Norms){
            results[cursor] = current;
            cursor++;
        }

        current = current->next;
    }
    results[number_finded] = NULL;
    
    free(norm.socketDiscussion);

    return results;
}

void printClient(Client* client, int client_number){
    printf("\n---------------------\n");
    if(client_number){
        printf("\nClient: %d\n", client_number);
    }
    else{
        printf("\nClient:\n");
    }
    printf("\tIP: %s\n",client->ip);
    printf("\tUsername: %s\n",client->login);
    printf("\tSocketDiscussion: %d\n",client->socketDiscussion);
    printf("\n---------------------\n");
}

char *replace_str(char *str, char *orig, char *rep)
{
    
  static char buffer[4096];
  char *p;

  if(!(p = strstr(str, orig)))  // Is 'orig' even in 'str'?
    return str;

  strncpy(buffer, str, p-str); // Copy characters from 'str' start to 'orig' st$
  buffer[p-str] = '\0';

  sprintf(buffer+(p-str), "%s%s", rep, p+strlen(orig));
  return buffer;
}

void printLlClient(Client* ptrLlClient){

    Client* current;
    int client_number;

    client_number = 0;
    current = ptrLlClient;

    while(current){
        client_number++;
        printClient(current,client_number);
        current = current->next;
    }
}

SearchStruct createSearchStruct(char* username_Wanted, char* ip_Wanted,int socketDiscussion){

    SearchStruct norm;
     if(username_Wanted){
        norm.login = username_Wanted;
    }
    else{
        norm.login = NULL;
    }
    if(ip_Wanted){
        norm.ip = ip_Wanted;
    }
    else{
        norm.ip = NULL;
    }
    if(socketDiscussion >= 0){
        
        norm.socketDiscussion = malloc(sizeof(int));
        *(norm.socketDiscussion) = socketDiscussion;
    }
    else{
        norm.socketDiscussion = NULL;
    }
    return norm;

}

int setClientLogin(Client* client, char* newLogin, Client* ptrLlClient){
    
    Client* current = ptrLlClient;
    int is_taken = 0;
    while(current){

        if(!strcmp(newLogin,current->login)){
            is_taken = 1;
            break;
        }

        current = current->next;
    }
    if(is_taken == 0){
        //regex
        
        //only letters and numbers
        if( match(newLogin, "[^A-Za-z0-9]+") != 0){
            return -2;
        }

        //reserved names
        if(!strcasecmp(newLogin,"SYSTEM") || !strcasecmp(newLogin,"chris") || !strcasecmp(newLogin,"FMCofficiel") || !strcasecmp(newLogin,"ERROR") || !strcasecmp(newLogin,"PRIVATE")){
            return -3;
        }

        strcpy(client->login,newLogin);
        return 1;       
    }
    else{
        //Error code -1 -> already taken
        return -1;
    }
}


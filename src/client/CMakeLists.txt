# minimum version of cmake required
cmake_minimum_required(VERSION 3.0.0)
# name of the project built
project(client)


# find OpenGL
find_package(OpenGL REQUIRED)
# find GLUT
find_package(GLUT REQUIRED)

# Verify that OpenGL is available
if(NOT ${OPENGL_FOUND})
    message("OPENGL not found")
endif()

if(NOT ${GLUT_FOUND})
    message("GLUT not found")
endif()

# add GLUT and OpenGL include directories
include_directories( 
    ${GLUT_INCLUDE_DIR}
    ${OPENGL_INCLUDE_DIRS} 
    ${X11_INCLUDE_DIRS}   
)

## [BEGIN_BUILD_LIBISENTLIB]

# add all sources in GfxLib/
file(GLOB SOURCES "../../cigma/*.c")
file(GLOB HEADERS "../../cigma/*.h")

# create static library "libisentlib" from SOURCES
add_library(lcigma STATIC ${SOURCES} ${HEADERS})
set( LCIGMA lcigma)
## [END_BUILD_LIBISENTLIB]


## [BEGIN_BUILD_LIBKYLLIANLIB]

# add all sources in Lib/
file(GLOB SOURCES "../../lib/*.c")
file(GLOB HEADERS "../../include/client/*.h")

# create static library "libkyllianlib" from SOURCES
add_library(kyllianlib STATIC ${SOURCES} ${HEADERS})
#set a surname to use it after
set( LIBKYLLIANLIB kyllianlib)

## [END_BUILD_LIBKYLLIANLIB]


## [BEGIN_BUILD_IHM_TEST]

# create an executable called client 
# main program : pingu-client.c
# add the following source files after
add_executable(client pingu-client.c pingu-client.h)
# link the static libraries with the main program
# m means "mathlib" or -lm
# OPENGL is -lGL
# -lX11 for X11
target_link_libraries(client
    ${LIBKYLLIANLIB}
    ${LCIGMA}
    ${OPENGL_LIBRARY}
    ${GLUT_LIBRARY}
    m
    X11
	SDL 
	csfml-audio 
	csfml-system 
	SDL_image 
	freetype
)
# add -Wall in case
target_compile_options(client PRIVATE -Wall)

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "../../include/client/cgraphs.h"
#include "../../include/client/geometry.h"
#include "../../include/client/cssparser.h"
#include "../../include/client/Cwing.h"
#include "../../include/client/consoletools.h"
#include "../../include/client/KyllianToolsLib.h"
#include "../../include/client/networking.h"

#include "pingu-client.h"

int idWindow;
char* global_addr;
int global_port;
char* global_name;

int main(int argc,char** argv){
	int largeurFenetre 	   = 1200;
	int hauteurFenetre 	   = 800;
	int xfenetre 		   = 800;
	int yfenetre 		   = 600;
	char *nomFenetre 	   = "Pingu";
	int limFPS 			   = 1000;
	void (*callbacks[6])() = {initialize,update,render,NULL,NULL,windowClose};

	global_port = 5689;
	global_addr = str_format("127.0.0.1");
	global_name = NULL;
	if(argc > 1){
		global_addr = str_format(argv[1]);
	}
	if(argc > 2){
		global_port = atoi(argv[2]);
	}
	if(argc > 3){
		global_name = str_format("%s",argv[3]);
	}

	setDebugMode(false); 		//Affichage des messages de debug ?
	setIgnoreError(false);		//Ignorer les erreurs ?

	idWindow = startGFX(largeurFenetre,hauteurFenetre,xfenetre,yfenetre,nomFenetre,limFPS,callbacks,&argc,argv);
	return 0;
}

Date new_Date (short hour,short minute,short seconds,short day,short month,short year);
Date getDateNow ();
User new_User(char* nom);
Message new_Message (char* content,User user,Date date);
void afficheMessage (Message* m,int x,int y);
ScrollPane new_ScrollPane(int width,int height,int ymax,int scrollspeed);
void updateScrollPane (ScrollPane* s);
void afficheFondScrollPane (ScrollPane* scrollp,int x,int y,int width,int height);
void afficheBarScrollPane (ScrollPane* scrollp);
User** parseUserList (char* strlist);
User** getUserList(void);
void addMessage (Message m);
void actionOfBtnHello();
void actionOfBtnVersion();
void actionOfBtnSelectAll();
void actionOfBtnQuit();
void actionOfBtnSend();
void analyzeMessage (char* message);
 
char* css = "../ressources/styles.css";

#define CHAT_FONT helvetica_neue_med
#define METADATA_FONT helvetica_neue_med
#define USERS_FONT helvetica_neue_med

FontText* helvetica_neue_med;
Overlay overlay;
User me;
Textfield txt;
Button btn_send;
ServerCom com;
Button btn_version;
Button btn_hello;
Button btn_quit;
Button btn_select_all;
Sound notifsound;

Message** msg_list;
User** user_list;

int xrect = 0, yrect = 0;
ScrollPane scrollp;


void initialize (){
	helvetica_neue_med = new_FontText("../ressources/fonts/HelveticaNeueMed.ttf",30);
	notifsound = new_Sound("../ressources/penguinChirp.ogg");
	overlay = new_Overlay_CSS("../ressources/pingu32.png",css,"overlay");
	com = new_ServerCom(global_addr,global_port);
	txt = new_Textfield_CSS(css,"sendfield");

	overlay.activate(&overlay);
	setIcon("../ressources/pingu64.png");

	btn_send 		= new_Button_CSS("Send",		css, "sendbutton"	);
	btn_version 	= new_Button_CSS("Version",		css, "statusbutton"	);
	btn_hello 		= new_Button_CSS("Hello",		css, "statusbutton"	);
	btn_quit 		= new_Button_CSS("Quit",		css, "statusbutton"	);
	btn_select_all 	= new_Button_CSS("Select All",	css, "statusbutton"	);

	btn_send.addEventListener		(&btn_send,			"click",	actionOfBtnSend			);
	btn_version.addEventListener	(&btn_version,		"click",	actionOfBtnVersion		);
	btn_hello.addEventListener		(&btn_hello,		"click",	actionOfBtnHello		);
	btn_quit.addEventListener		(&btn_quit,			"click",	actionOfBtnQuit			);
	btn_select_all.addEventListener	(&btn_select_all,	"click",	actionOfBtnSelectAll	);

	scrollp = new_ScrollPane(500,HF,HF,50);
	scrollp.color = (RGB) {50,50,50};
	scrollp.bar_color = (RGB) {80,80,80};

	me = new_User("myself");
	msg_list = malloc(sizeof(Message*));
	msg_list[0] = NULL;

	//initialisation avec un exemple de retour de la commande list
	char* userstr = (char *) malloc(sizeof(char)*strlen("myself|"));
	strcpy(userstr,"myself|");
	user_list = parseUserList(userstr);

	free(userstr);
	userstr = NULL;
	if(global_name != NULL){
		char* command = str_format("!login %s.",global_name);
		println(command);
		envoieMessage(command,com);
	}
	DEBUGHERE;
}

void update (){
	DEBUGHERE;
	overlay.update(&overlay);
	ifleftclickup(){
		int PanWidth = LF/5;
		for(int i=0;i<count(user_list);i++){
			if(pointIsInRectangle(AS,OS,0,HF - 50*(i+1)-25,PanWidth,50)){
				user_list[i]->selected = !user_list[i]->selected;
			}
		}
	}

	ifkeyup(13){	//Touche ENTER
		if(txt.isEditing){
			actionOfBtnSend();
		}
		keyboardState[13] = false;	//on catch la touche echap
	}
	txt.update(&txt);

	analyzeMessage(getMessage(com));

	btn_send		.update(&btn_send		);
	btn_hello		.update(&btn_hello		);
	btn_quit		.update(&btn_quit		);
	btn_version		.update(&btn_version	);
	btn_select_all	.update(&btn_select_all	);

	updateScrollPane(&scrollp);
	DEBUGHERE;
	if(!overlay.movewindow)
		updateGraphics();
}

void render (){
	DEBUGHERE;
	clear(50,50,50);

	int PanWidth = LF/5;
	int TextStart = PanWidth + scrollp.width/2 - ratioValue(txt.style.width,LF)/2 - ratioValue(btn_send.style.width,LF)/2 - 20;
	int TextHeight = 20;

	strokeWidth(0);
	afficheFondScrollPane(&scrollp,PanWidth,0,LF-PanWidth,HF);
	int over = 70;
	foreach(Message** mess,msg_list){
		int x = 10;
		strokeWidth(2);
		if(!strcmp((*mess)->user.nom,me.nom)){		//c'est mon message
			fill(0,153,155,255);
			x = scrollp.width - 10 - 25 - 55 - getTextWidth(CHAT_FONT,15,(*mess)->content);
		}
		else{
			fill(0,120,200,255);
			stroke(0,120,200,255);
			char* metadata = str_format("%s:      [%d:%d]",(*mess)->user.nom,(*mess)->date.hour,(*mess)->date.minute);
			text(METADATA_FONT,scrollp.x + x+20,scrollp.height+scrollp.yc-over+38,10,metadata);
		}
		strokeWidth(0);
		afficheMessage(*mess,scrollp.x + x,scrollp.height+scrollp.yc-over);
		over+=50;
	}

	afficheBarScrollPane(&scrollp);

	//Affihage du panneau Utilisateurs connectés
	fill(60,60,60,255);
	strokeWidth(0);
	rectangle(0,0,PanWidth,HF);

	for(int i=0;i<count(user_list);i++){
		if(user_list[i]->selected){
			fill(80,80,80,255);
			rectangle(0,HF-50*(i+1)-25,PanWidth,50);
		}
		fillRGB(COLOR_WHITE);
		int x = PanWidth/2 - getTextWidth(USERS_FONT,18,user_list[i]->nom)/2;
		int y = HF-50*(i+1);
		text(USERS_FONT,x,y,18,user_list[i]->nom);
	}

	//Affichage du textField
	fill(50,50,50,255);
	rectangle(PanWidth,0,LF-25-PanWidth,60);
	txt.show(&txt,TextStart,TextHeight);
	btn_send.show(&btn_send,TextStart + ratioValue(txt.style.width,LF) + 20,TextHeight);

	fill(60,60,60,255);
	strokeWidth(0);
	rectangle(0,HF-23,LF,23);
	int btnsel_w = ratioValue(btn_select_all.style.width,LF);

	btn_hello		.show(&btn_hello 		,PanWidth+10  			,HF-20 	);
	btn_version		.show(&btn_version 		,PanWidth+80  			,HF-20 	);
	btn_quit		.show(&btn_quit 		,PanWidth+150 			,HF-20 	);
	btn_select_all	.show(&btn_select_all	,PanWidth/2-btnsel_w/2	,20	 	);


	stroke(125,125,125,255);
	int x = PanWidth/2-getTextWidth(GLUT_BITMAP_HELVETICA_12,12,"Users in selection")/2;
	int y = HF-15;
	text(GLUT_BITMAP_HELVETICA_12,x,y,12,"Users in selection");

	overlay.show(&overlay);
	DEBUGHERE;
}

void windowClose (){
	freeSound(&notifsound);
	directeur("!quit",com);
	int i=0;
	while(msg_list[i]!= NULL){
		printf("Liberation message %d...\n",i);
		free(msg_list[i]);
		msg_list[i]=NULL;
		i++;
	}
	free(msg_list);
	msg_list = NULL;
	send(com.sock_desc,"!quit",sizeof("!quit"),0);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void analyzeMessage (char* message){
	if(strcmp(message,"")){
		if(!(strcmp(firstWord(message),"!message"))){
			
			char *msg = retireDebutChaine(message,strlen("!message "));
			char* username = firstWord(msg);
			printf("message analysé : '%s'\n",msg);
			printf("username : '%s'\n",username);
			char* tmp = msg;
			msg = retireDebutChaine(tmp,strlen(username));
			free(tmp);
			addMessage(new_Message(msg,new_User(username),getDateNow()));
			printf("message added ok\n");
			free(msg);
		
		}
		else if(!(strcmp(firstWord(message),"!version"))){
			char *msg = retireDebutChaine(message,strlen("!version "));
			addMessage(new_Message(msg,new_User("SYSTEM"),getDateNow()));
			free(msg);
		}
		else if(!(strcmp(firstWord(message),"!hello"))){
			char *msg = retireDebutChaine(message,strlen("!hello "));
			addMessage(new_Message(msg,new_User("SYSTEM"),getDateNow()));
			free(msg);
		}
		else if(!(strcmp(firstWord(message),"!list"))){
			char *msg = retireDebutChaine(message,strlen("!list "));
			user_list = parseUserList(msg);
			free(msg);
		}
		else if(!(strcmp(firstWord(message),"!error"))){
			char *msg = retireDebutChaine(message,strlen("!error "));
			addMessage(new_Message(msg,new_User("ERROR"),getDateNow()));
			free(msg);
		}
		else{
			addMessage(new_Message(message,new_User("SYSTEM"),getDateNow()));
		}

	}
}

Date new_Date (short hour,short minute,short seconds,short day,short month,short year){
	Date ret;
	ret.hour = hour;
	ret.minute = minute;
	ret.seconds = seconds;
	ret.day = day;
	ret.month = month;
	ret.year = year;
	return ret;
}

Date getDateNow (){
	time_t rawtime;
  	struct tm * timeinfo;
  	time ( &rawtime );
  	timeinfo = localtime ( &rawtime );
	return new_Date(timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec,timeinfo->tm_mday,timeinfo->tm_mon,timeinfo->tm_year);
}

User new_User(char* nom){
	User ret;
	ret.nom = nom;
	ret.selected = true;
	return ret;
}

Message new_Message (char* content,User user,Date date){
	Message ret;
	ret.content = malloc(sizeof(char)*(strlen(content)+1));
	memcpy(ret.content,content,strlen(content));
	ret.content[strlen(content)] = '\0';
	ret.user = user;
	ret.date = date;
	return ret;
}

void afficheMessage (Message* m,int x,int y){
	roundedRectangle(x,y,getTextWidth(CHAT_FONT,15,m->content) +60 ,35,1,50);
	if(!strcasecmp("SYSTEM",m->user.nom)){
		printdebug("'%s'",m->content);
		stroke(135,206,250,255);
	}
	else if(!strcasecmp("ERROR",m->user.nom)){
		stroke(210, 82, 82,255);
	}
	else if(!strcasecmp("PRIVATE",m->user.nom)){
		stroke(255, 198, 109,255);
	}
	else{
		stroke(255, 255, 255,255);
	}
	//afficheChaine(m->content,20,x+10,y+(35-20)/2);
	text(CHAT_FONT,x+30,y+(35-20)/2,15,m->content);
	////afficheBitChaine(CHAT_FONT,m->content,x+30,y+(35-20+glutBitmapWidth(CHAT_FONT,'A'))/2);
}


ScrollPane new_ScrollPane(int width,int height,int ymax,int scrollspeed){
	ScrollPane ret;
	ret.width = width;
	ret.height = height;
	ret.x = 0;
	ret.y = 0;
	ret.scrollspeed = scrollspeed;
	ret.y0 = 0;
	ret.yc = 0;
	ret.ymax = ymax;
	ret.color = (RGB){0,0,0};
	ret.bar_color = (RGB) {255,255,255};
	return ret;
}

void updateScrollPane (ScrollPane* s){
	if(handleSouris){
		if(pointIsInRectangle(AS,OS,s->x,s->y,s->width,s->height)){
			if(mouseState == MOUSE_SCROLL_DOWN){
				if(s->yc + s->scrollspeed + s->height <= s->ymax ){
					s->yc += s->scrollspeed;
				}
				else{
					s->yc = s->ymax - s->height;
				}
			}
			if(mouseState == MOUSE_SCROLL_UP){
				if(s->yc - s->scrollspeed >= s->y0){
					s->yc -= s->scrollspeed;
				}
				else{
					s->yc = s->y0;
				}
			}
		}
	}
}

void afficheFondScrollPane (ScrollPane* scrollp,int x,int y,int width,int height){
	fillRGB(scrollp->color);
	scrollp->x = x;
	scrollp->y = y;
	scrollp->width = width;
	scrollp->height = height;
	rectangle(scrollp->x,scrollp->y,scrollp->width,scrollp->height);
}

void afficheBarScrollPane (ScrollPane* scrollp){
	fillRGB(scrollp->bar_color);
	roundedRectangle(
		scrollp->x+scrollp->width-18,
		(scrollp->ymax - scrollp->height - scrollp->yc)*scrollp->height/scrollp->ymax,
		18,
		scrollp->height*scrollp->height / scrollp->ymax,
		1,
		40
	);
}

User** parseUserList (char* strlist){
	char** userliststr = str_split(strlist,'|');
	int nb = count(userliststr)-1;
	User** ret = malloc(sizeof(User*)*(nb+1));
	for(int i = 0;i<nb;i++){
		ret[i] = malloc(sizeof(User));
		*(ret[i]) = new_User(userliststr[i+1]);
	}
	ret[nb] = NULL;
	return ret;
}

User** getUserList(void){
	return user_list;
}

void addMessage (Message m){
	int nb = count(msg_list);
   	msg_list = realloc(msg_list,sizeof(Message*)*(nb+2));
   	msg_list[nb] = malloc(sizeof(Message));
   	*(msg_list[nb]) = m;
	msg_list[nb+1] = NULL;
	if(nb*50 > HF-100){
		scrollp.ymax += 50;
	}
	scrollp.yc = scrollp.ymax - HF;
	if(!str_same(m.user.nom,me.nom)){
		notifsound.play(&notifsound);
	}
}

void actionOfBtnHello(){
	//gerer la commande !hello
	directeur("!hello",com);
}

void actionOfBtnVersion(){
	//gerer la commande !version
	directeur("!version",com);
}

void actionOfBtnSelectAll(){
	//selectionner tous les user ou tous les desellectionner
	bool allselected = true;
	foreach(User** tmp,user_list){
		User* user = *tmp;
		if(!user->selected){
			allselected = false;
		}
	}
	foreach(User** tmp,user_list){
		User* user = *tmp;
		user->selected = !allselected;
	}
}

void actionOfBtnQuit(){
	//gerer la commande !quit
	endProcess();
}

void actionOfBtnSend(){
	//partie reseau mise en commentaire pour le test local
	if(txt.text != NULL && strcmp(txt.text,"") ){
		directeur(txt.text,com);
		addMessage(new_Message(txt.text,me,getDateNow()));
		strcpy(txt.text,"");
	}
}
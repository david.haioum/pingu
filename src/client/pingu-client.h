

// STRUCTURES
typedef struct Date{
	short day,month,year;
	short hour,minute,seconds;
}Date;

typedef struct Message{
	char* content;
	User user;
	Date date;
}Message;

typedef struct ChatRoom{
	User* users;
	int nb_user;
	Date create_date;
	Message* msg;
	int nb_message;
}ChatRoom;

typedef struct ScrollPane {
	int width;
	int height;
	int x,y;
	int scrollspeed;
	RGB color;
	RGB bar_color;

	int yc;
	int y0;
	int ymax;
}ScrollPane;


//GlOBALES EXTERNES 



// FONCTIONS


//callback apres la création de la fenetre
void initialize ();

//callback appelé a chaque frame pour le traitement
void update ();

//callback appelé pour dessiner la frame suivante
void render ();

//callback fenetre redimensionnée
void windowResize();

//callback souris bougée
void mouseMoved ();

void windowClose();
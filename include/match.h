#ifndef MATCH_H
#define MATCH_H

int match(const char *string, const char *pattern);

#endif /* MATCH_H */
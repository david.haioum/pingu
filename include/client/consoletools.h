#ifndef STDBOOL_H
    #include <stdbool.h>
#endif

#define TXT_TYPE_BOLD "\e[1"

#define TXT_COLOR_YELLOW "\x1b[33m"
#define TXT_COLOR_BLACK "\x1b[30m"
#define TXT_COLOR_GREEN "\x1b[32m"
#define TXT_COLOR_PURPLE "\x1b[35m"
#define TXT_COLOR_LIGHT_BLUE "\x1b[36m"
#define TXT_COLOR_DARK_BLUE "\x1b[34m"
#define TXT_COLOR_RED "\x1b[31m"
#define TXT_COLOR_END "\x1b[0m"

/**
 * @brief affiche une chaine en mode debug (avec fichier,fonction et numero de la ligne ou cette fonction est appelée)
 * n'affichera rien si la globale debug_mode == false
 */
#define printdebug(...) printdebug2 (__FILE__,__LINE__,__func__,__VA_ARGS__)
#define DEBUGSTART printdebug2 (__FILE__,__LINE__,__func__,"ENTERING FUNCTION")
#define DEBUGEND printdebug2 (__FILE__,__LINE__,__func__,"LEAVING FUNCTION")
#define DEBUGHERE printdebug2 (__FILE__,__LINE__,__func__,"REACHED THIS POINT")

void setDebugMode (bool val);
void setIgnoreError (bool val);
bool getDebugMode ();
bool getIgnoreError ();
void printlncolored (char* color,const char *format, ...);
void printyellow (const char *format, ...);
void printgreen (const char *format, ...);
void printpurple (const char *format, ...);
void printlightblue (const char *format, ...);
void printdarkblue (const char *format, ...);
void printred (const char *format, ...);
void println (const char *format, ...);
void printdebug2 (char* filename,int line,const char* func,const char *format, ...);
void printerr (const char *format, ...);
void printPalette();
#define NOIR 0,0,0
#define BLANC 255,255,255

#define PI 3.14159265358979323846
#define count(x) count2((void**)x)
#define betterFree(x) betterFree2((void**)x)

#define foreach(val,tab) \
	for(int foreach_cpt = 0,foreach_size = count((void**)tab),foreach_keep = 1;\
	foreach_keep && foreach_cpt < foreach_size; \
	foreach_keep = !foreach_keep, foreach_cpt++) \
	for(val = (tab) + foreach_cpt; foreach_keep ; foreach_keep = !foreach_keep)

#define typename(x) _Generic((x),                                                 \
        _Bool: "_Bool",                  unsigned char: "unsigned char",          \
         char: "char",                     signed char: "signed char",            \
    short int: "short int",         unsigned short int: "unsigned short int",     \
          int: "int",                     unsigned int: "unsigned int",           \
     long int: "long int",           unsigned long int: "unsigned long int",      \
long long int: "long long int", unsigned long long int: "unsigned long long int", \
        float: "float",                         double: "double",                 \
  long double: "long double",                   char *: "pointer to char",        \
       void *: "pointer to void",                int *: "pointer to int",         \
      default: "other")

bool str_same (char* str1,char* str2);
int str_substringIndex (char* source,char* substr);
char* str_removeAll(char* src,char target);
int parseInt (char* str);
float parseFloat (char* str);
double min (double a,double b);
double max (double a,double b);
char** str_split(char* a_str, const char a_delim);
int getDbText (char* fcontents,char* filename);
int count2 (void** T);
char* str_replaceAll(char* src/*,char* target,char* replacement*/);
char* fileGetText (char* filename);
char* fileGetExtension (char* path);

char* runProcess (char* command,char* logs_location);

//retourne le nombre d elements initialisés dans un tableau bidimensionnel d entier
int trueSizeOfIntTab (int **T);
//cree par Kyllian MARIE

//retourne le nombre d elements initialisés dans un tableau d entier
int trueSizeOfInt (int *T);
//cree par Kyllian MARIE

//retourne true si un fichier donné existe
bool fileExist (char* nomFichier);
//cree par Kyllian MARIE

//fclose seulement si le fichier != NULL
void betterfclose (FILE* f);
//cree par Kyllian MARIE

//free seulement si le pointeur != NULL
void betterFree2 (void **ptr);
//cree par Kyllian MARIE

//permet de malloc un tableau a deux dimensions
void** bigMalloc (int size,int DIM1,int DIM2);
//cree par Kyllian MARIE

//permet de generer une chaine contenant un chemin vers un fichier donné avec un nombre aleatoire
char* genereCheminAleatoire(int nbFichier,char* path,char* extension);
//cree par Kyllian MARIE

bool not (bool a);

bool isValInArray (void** tab,void* val);
char* str_format (char* format,...);

float randFloat ();
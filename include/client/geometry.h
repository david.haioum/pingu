#ifndef K_GEO_LIB
#define K_GEO_LIB
typedef struct Point_2f{
	float x,y;
}Point_2f;

typedef struct Point{
	int x,y;
}Point;

typedef struct Ligne{
	Point p1,p2;
}Ligne;

typedef struct Polygone{
	Point* pts;
	int nbpts;
	int width;
	int height;
}Polygone;

bool pointIsInRectangle (int x,int y,int r_pos_x,int r_pos_y,int r_width,int r_height);
Polygone initPolygone (int width,int height,float coef,float rnbpts);
void dessinePolygone (Polygone P,int x,int y);
bool pointIsInCircle (int x,int y,int xc,int yc,int r);
void dessinePolygoneZoom (Polygone P,int x,int y,float zoom);
void freePolygone(Polygone p);
Point** makeLine (int xa, int ya, int xb, int yb);
Point** Point_cat(Point** p1,Point** p2);

Point new_Point (int x,int y);
Point_2f new_Point_2f (float x,float y);
Ligne new_Ligne (int x1,int y1,int x2,int y2);

char* Point_tostring (Point p);
char* Point_2f_tostring (Point_2f p);
char* Ligne_tostring (Ligne l);
char* Polygone_tostring (Polygone p);

#endif
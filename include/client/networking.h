#define LG_MESSAGE 1000
#define PF_INET 2

typedef struct sockaddr_in sockaddr_in;

typedef struct ServerCom{
   bool ouvert;
	int sock_desc;
	sockaddr_in sock_addr_in;
   socklen_t addr_len;
}ServerCom;

ServerCom new_ServerCom (char* ip,int port);
void envoieMessage (char* message,ServerCom com);
char* getMessage (ServerCom com);
void termineConnection (ServerCom* com);

typedef struct GlobalInfos{
    char* version;
    char* name;
    char* hellomsg;
}GlobalInfos;

void directeur(char *chaine, ServerCom com);
bool isCommand(char *chaine);
char* firstWord(char *string);
char* retireDebutChaine(char *chaine, int index);
char* retireDernierchar(char *chaine);

typedef struct User{
	char* nom;
	bool selected;
}User;

User** getUserList(void);
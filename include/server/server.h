#ifndef SERVER_H

#define SERVER_H

#define PORT_ARG "-p"

#define SERVER_NAME_ARG "-n"

#define PASSWORD_ARG "-f"

#define HELP_ARG "--help"

#define EXPECTED_ARGUMENT_NUMBER 2

void initServer(Server* s,int argc, char* argv[]);

void processData(int socketDescriptor, Client** addPtrLlClient, char data[],int client_socket_in_chat[]);

void broadcastChat(char data[], int client_socket_in_chat[],Client* ptrLlClient, Client* exception);

void broadcastLobby(char data[], int client_socket_in_lobby[]);

void displayHelpCommand();

#endif


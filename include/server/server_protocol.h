#ifndef SERVER_PROTOCOLE_H
#define SERVER_PROTOCOLE_H

static const char SERVER_VERSION[] = "1";

#define QUIT_CMD "!quit"

#define VERSION_CMD "!version"

#define HELLO_CMD "!hello"

#define LOGIN_CMD "!login"

#define MESSAGE_CMD "!message"

#define LIST_CMD "!list"

#endif
#ifndef SERVER_STRUCTURES_H
#define SERVER_STRUCTURES_H

typedef struct Client{
    char* ip;
    char* login;
    int socketDiscussion;
    struct Client* next;
}Client;


/**
 * @brief Search Structure
 * 
 */
typedef struct SearchStruct{
    char* ip;
    char* login;
    int* socketDiscussion;
}SearchStruct;


typedef struct Server {
    char* name;
    int port;
    int sockfd;
    char password[256];
    struct Client* listeOfClient;
} Server;


#endif
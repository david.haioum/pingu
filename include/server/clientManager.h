#ifndef CLIENT_MANAGER_H
#define CLIENT_MANAGER_H

/**
 * @brief Create a Client object
 * 
 * @param login 
 * @param ip 
 * @param socketDescriptor
 * 
 * @return Client* 
 */
Client* createClient(char* login, char* ip, int socketDescriptor);


/**
 * @brief Add a Client to the linked list
 * 
 * @param addrptrLlClient 
 * @param newClient
 * 
 * @return codeError
 * 
 */
int addClient(Client** addrptrLlClient,Client* newClient);


/**
 * @brief Remove a Client to the linked list
 * 
 * @param addPtrLlClient 
 * @param Client 
 * 
 * @return codeError
 * 
 */
int removeClient(Client** addPtrLlClient,Client* client);

/**
 * @brief Count the number of Clients
 * 
 * @param ptrLcClient
 * 
 * @return codeError
 * 
 */
int countLlClient(Client* ptrLcClient);

/**
 * @brief Free the Client Linked List
 * 
 * @param addrptrLlClient
 * 
 * @return codeError
 */
int freeLinkedList(Client** addrptrLlClient);


/**
 * @brief Find Client(s) in the linked list according to the search parameters
 * 
 * @param ptrLlClient 
 * @param norm 
 * 
 * @return Client* 
 */
Client** findClient(Client* ptrLlClient, SearchStruct norm);

/**
 * @brief Print Client informations
 * 
 * @format start
 * 
 * Client n°X:
 *     IP: X.X.X.X
 *     Username: XXXX
 * 
 * @format end
 * 
 * @param client 
 * @param client_number (default: 0)
 * 
 */
void printClient(Client* client, int client_number);

/**
 * @brief Print Client Linked List with printClient format
 * 
 * @param ptrLcClient 
 */
void printLlClient(Client* ptrLlClient);

/**
 * @brief Create a Search Struct object | To find everybody, put @param1 and @param2 at NULL and @param3 at negative integer
 * 
 * @param username_Wanted 
 * @param ip_Wanted 
 * @param socketDescriptor
 * 
 * @return SearchStruct 
 */
SearchStruct createSearchStruct(char* username_Wanted, char* ip_Wanted, int socketDescriptor);

/**
 * @brief setter for login
 * 
 * @param client 
 * @param newLogin 
 * 
 * @return codeError 
 */
int setClientLogin(Client* client, char* newLogin, Client* ptrLlClient);

char *replace_str(char *str, char *orig, char *rep);


#endif
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>

#include "../include/client/consoletools.h"
#include "../include/client/networking.h"

ServerCom new_ServerCom (char* ip,int global_port){
	ServerCom ret;
   println("port utilisé : %d",global_port);
	ret.sock_desc = socket(PF_INET, SOCK_STREAM, 0);
   if(ret.sock_desc < 0){
      perror("socket");
      exit(-1);
   } 
   printf("Socket créée avec succès ! (%d)\n", ret.sock_desc);
   ret.addr_len = sizeof(sockaddr_in);
   memset(&(ret.sock_addr_in), 0x00, ret.addr_len);
   ret.sock_addr_in.sin_family = PF_INET;
   ret.sock_addr_in.sin_port = htons(global_port);
   inet_aton(ip, &(ret.sock_addr_in.sin_addr)); // à modifier selon ses besoins
   // Débute la connexion vers le processus serveur distant
   if((connect(ret.sock_desc, (struct sockaddr *)&(ret.sock_addr_in), ret.addr_len)) == -1)
   {
      perror("connect"); // Affiche le message d'erreur
      close(ret.sock_desc); // On ferme la ressource avant de quitter
      ret.ouvert = false;
      exit(-2); // On sort en indiquant un code erreur
   }
   
   struct timeval timeout;      
   timeout.tv_sec = 0;
   timeout.tv_usec = 50;

   if (setsockopt (ret.sock_desc, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,
               sizeof(timeout)) < 0)
      perror("setsockopt failed\n");

   if (setsockopt (ret.sock_desc, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout,
               sizeof(timeout)) < 0)
      perror("setsockopt failed\n");
   printf("Connexion au serveur réussie avec succès !\n");
   ret.ouvert = true;
   return ret;
}

void envoieMessage (char* message,ServerCom com){
   int retour_serveur = send(com.sock_desc, message, strlen(message),0); // message à TAILLE variable
   if(retour_serveur != -1){
      switch(retour_serveur){
         case -1 : /* une erreur ! */
            perror("write");
            close(com.sock_desc);
            exit(-3);
            break;
         case 0 : /* la socket est fermée */
            fprintf(stderr, "La socket a été fermée par le serveur !\n\n"); 
            close(com.sock_desc);
            exit(-1);
            break;
         default: /* envoi de n octets */
            // fin de l'étape n°4 !   
            printf("Message '%s' envoyé avec succès (%d octets)\n\n", message, retour_serveur); 
            break;
      }
   }
}

char* getMessage (ServerCom com){
   char* messageRecu = (char *) malloc(sizeof(char)*LG_MESSAGE);
   int lus = read(com.sock_desc, messageRecu, LG_MESSAGE*sizeof(char)); /* attend un message de TAILLE fixe */
   
   if(lus != -1){
      switch(lus){
         case -1 : /* une erreur ! */
            perror("read");
            close(com.sock_desc);
            exit(-4);
         case 0 : /* la socket est fermée */
            fprintf(stderr, "La socket a été fermée par le serveur !\n\n");
            close(com.sock_desc);
            exit(-1);

         default: /* réception de n octets */
            // fin de l'étape n°4 !   
            if(!strcmp(messageRecu,"!version")){
               send(com.sock_desc, "!version 1", sizeof("!version 1"),0);
            }
            else{
               printf("\nmessage reçu: '%s'\n",messageRecu);
               send(com.sock_desc, "", sizeof(""),0);
               return messageRecu;
            }
      }
      return "";
   }
   return "";
}

void termineConnection (ServerCom* com){
   if(com->ouvert){
      close(com->sock_desc);
      com->ouvert = false;
   }
}
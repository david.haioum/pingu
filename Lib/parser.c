#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "../include/client/networking.h"
#include "../include/client/KyllianToolsLib.h"

typedef int boolean;
typedef char Login[101];

//Prototypes
void prompt(char* string);
void messageTextuel(char *msg, int msg_size, ServerCom com);

typedef enum commande {DEFAULT, VERSION, NAME, GREETINGS, QUIT, LOGIN, LOGIN_CHANGE, LIST, MESSAGE} commande;

/**
 * @brief Renvoie 1 si commande, 0 si message
 * 
 * @param chaine : a analyser
 * @return true : commande
 * @return false : message
 */
bool isCommand(char *chaine){
    bool state;
    if(chaine[0] == '!'){
        //Cas de traitement d'une commande.
        state = true;
    }
    else{
        //Cas de traitement d'un message
        state = false;
    }
    return state;
}

/**
 * @brief Retire le premier caractère d'une chaine
 * 
 * @param chaine 
 * @return char* 
 */
char* retire1erChar(char *chaine){
    char* retour = (char*) malloc((strlen(chaine)-1) * sizeof(char));
    int num = 1;

    while(num < strlen(chaine)){
        retour[num-1] = chaine[num];
        num++;
    }
    retour[num-1]= '\0';

    return retour;
}

/**
 * @brief Retire une partie de debut de chaine
 * @param chaine
 * @param index : longueur a retirer
 */

char* retireDebutChaine(char *chaine, int index){
    int size = strlen(&chaine[index])+1;
    char* retour = malloc(sizeof (char) * size);
    memcpy(retour,chaine + index,size-1);
    retour[size-1] = '\0';
    
    return retour;
}

/**
 * @brief Retire le dernier caractere d'une chaine.
 * @return la chaine traitée
 */
char* retireDernierchar(char *chaine){
    printf("\nREcu: '%s'\n",chaine);
    char* retour = (char*) malloc((strlen(chaine)-1) * sizeof(char));
    int num = strlen(chaine)-1;

    for(int i=0; i<num;i++){
        retour[i] = chaine[i];
    }
    retour[num] = '\0';
    return retour;
}

/**
 * @brief Retourne false si aucun mot, true si correct.
 * Attention : l'entier renvoyé par la fonction correspond au nombre de mots en plus du mot de base.
 * ex : pour une chaine "!get something from database" on retournera 3.
 */
int manyWords(char* string){
    char c;
    int i=0;
    int words=0;
    do{
        c = string[i];
        i++;
        if(c == ' ' && string[i+1]!='\0'){
            words += 1;
        }
    }while( c != '\0');

    return words;
}

/**
 * @brief Retourne le premier mot (char*) d'une chaine
 * @param chaine à traiter
 * 
 */
char* firstWord(char *string){
    int tmpSize = strlen(string);
    char tmp[tmpSize];
    strcpy(tmp,string);
    char* retour = malloc(sizeof(char)*(strlen(strtok(tmp," "))+1));
    strcpy(tmp,string);
    int size = strlen(strtok(tmp," "));
    strcpy(tmp,string);
    memcpy(retour,strtok(tmp," "),size);
    retour[size] = '\0';
    return retour;
}

/**
 * @brief retourne la constante correspondant à la commande analysée
 * @param char* string : chaine à verifier
 */
commande checkCommandType(char *string){
    if(manyWords(string)){
        char *s = malloc(strlen(firstWord(string)+1*sizeof(char)));
        strcpy(s,firstWord(string));

        if(strcmp(s, "login")==0){
            return LOGIN_CHANGE;
        }
        else if(strcmp(s, "ping")==0 || strcmp(s, "message")==0 ){
            return MESSAGE;
        }
    }
    else{
        if(strcmp(string, "login")==0){
            return LOGIN;
        }
        if (strcmp(string,"version")==0){
        return VERSION;
        }
        else if (strcmp(string,"name")==0){
            return NAME;
        }
        else if (strcmp(string,"hello")==0){
            return GREETINGS;
        }
        else if (strcmp(string,"quit")==0){
            return QUIT;
        }
        else if (strcmp(string,"list")==0){
            return LIST;
        }
    }

    //Cas d'echec.
    return DEFAULT;
}

/**
 * @brief Donne les directives de saisie
 * 
 * @param chaine 
 */
void directeur(char *chaine, ServerCom com){
    char* bufferCmd;
    if( isCommand(chaine)){
        //Cas de traitement d'une commande.
        bufferCmd = malloc((strlen(chaine)-2)*sizeof(char));
        bufferCmd = retire1erChar(chaine);
        printf("c'est une commande !\n");
        printf("commande : '%s'\n",bufferCmd);
        /* On traite une entree utilisateur commencant par !commande -> commande */
        commande command = checkCommandType(bufferCmd);
            switch (command)
            {
            case LOGIN:
                envoieMessage("!login\n",com);
                break;

            case LOGIN_CHANGE:
                //Commande deja gérée coté serveur
                printf("Vous avez demandé un changement de pseudo...");
                envoieMessage(str_format("%s\n",chaine),com);
                break;
            
            case VERSION:
                envoieMessage("!version\n",com);
                break;
            
            case GREETINGS:
                envoieMessage("!hello\n",com);
                break;

            case NAME:
                envoieMessage("!name\n",com);
                break;

            case MESSAGE:
                /* code */
                printf("message command\n");
                envoieMessage(str_format("%s\n",chaine),com);
                break;

            case LIST:
                /* code */
                envoieMessage("!list\n",com);
                break;
            

            default:
                break;
            }
            free(bufferCmd);
            bufferCmd = NULL;
        }

    else{
        //C'est un message
        if(sizeof(chaine)!=0)
        messageTextuel(chaine,sizeof(chaine),com);
    }
}

void messageTextuel(char *msg, int msg_size, ServerCom com){
    int i = 0; int nbdest=0;
    User** List = getUserList();
    while(List[i] != NULL){
        if(List[i]->selected == 1){
            nbdest++;
        }
        i++;
    }
    char *buffer = malloc(sizeof(char)*(13+msg_size+101));
    if(nbdest > 0){
        if(nbdest == i){
            strcpy(buffer,"!message * ");
            strcat(buffer,msg);
            strcat(buffer,"\n");

            envoieMessage(buffer,com);
        }
        else {
            i=0;
            while(List[i] != NULL){
                if(List[i]->selected == 1){
                    strcpy(buffer,"!message ");
                    strcat(buffer,List[i]->nom);
                    strcat(buffer, " ");
                    strcat(buffer,msg);
                    strcat(buffer,"\n");

                    printf("envoi: '%s'\n",buffer);
                    envoieMessage(buffer,com);
                }
                i++;
            }
        }
    }
    else if (i==0){
        strcpy(buffer,"!message * ");
        strcat(buffer,msg);
        strcat(buffer,"\n");

        envoieMessage(buffer,com);

    }
   else{
       printf("Aucun destinataire selectionné !\n");
    }

    free(buffer);
    buffer= NULL;
    
}

/**
 * @brief Get the Global Infos object file
 * Fonction d'affectation de la structure GlobalInfos.
 * Par T.Simon
 * 
 * Non utilisée au final - laissée pour integration d'informations à partir de fichier.
 * 
 * @param file2open 
 * @param infos 
 */
void getGlobalInfos(char* file2open, GlobalInfos* infos){
    FILE *f;
    char bufferName[200];
    char bufferVersion[50];
    char bufferHello[500];

    f = fopen(file2open, "rt");
    if(f == NULL)
    {
        printf("File FAILURE!\n");
    }
    else{
        fgets(bufferName,200,f);
        bufferName[strlen(bufferName)-1]= '\0';
        fgets(bufferVersion,50,f);
        bufferVersion[strlen(bufferVersion)-1] = '\0';
        fgets(bufferHello,500,f);
        bufferHello[strlen(bufferHello)-1] = '\0';
        fclose(f);

        infos->name = (char*) malloc( ( strlen(bufferName) - strlen("NAME:") )*sizeof(char) );
        for(int i=0; i<strlen(bufferName); i++){
            infos->name[i] = bufferName[i+strlen("NAME:")];
        }
        infos->version = (char*) malloc( ( strlen(bufferName) - strlen("PROTOCOL_VERSION:") )*sizeof(char) );
        for(int i=0; i<strlen(bufferVersion); i++){
            infos->version[i] = bufferVersion[i+strlen("PROTOCOL_VERSION:")];
        }
        infos->hellomsg = (char*) malloc( ( strlen(bufferName) - strlen("HELLO_MSG:") )*sizeof(char) );
        for(int i=0; i<strlen(bufferHello); i++){
            infos->hellomsg[i] = bufferHello[i+strlen("HELLO_MSG:")];
        }
    }
}
